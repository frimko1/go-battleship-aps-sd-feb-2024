package main

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"go-battleship/cmd/controller"
	"go-battleship/cmd/letter"
)

func TestParsePosition(t *testing.T) {
	actual := parsePosition("A1")
	expected := &controller.Position{Column: letter.A, Row: 1}

	assert.Equal(t, expected, actual)
}

func TestRandomEnemyFleet(t *testing.T) {
	initializeEnemyFleet()
	firstPosition := enemyFleet[2].GetPositions()[1].Row
	pow := make([]int, 100)
	for range pow {
		initializeEnemyFleet()
		newPosition := enemyFleet[2].GetPositions()[1].Row
		if firstPosition != newPosition {
			return
		}
	}
	assert.Equal(t, true, false)
}

func TestParsePosition2(t *testing.T) {
	//given
	var expected *controller.Position = controller.NewPosition(letter.B, 1)
	//when
	var actual *controller.Position = parsePosition("B1")
	//then
	assert.Equal(t, expected, actual)
}
