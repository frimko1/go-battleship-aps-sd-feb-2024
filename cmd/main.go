package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"go-battleship/cmd/console"
	"go-battleship/cmd/controller"
	"go-battleship/cmd/letter"
)

var (
	enemyFleet []*controller.Ship
	enemyField *controller.Field
	myFleet    []*controller.Ship
	myField    *controller.Field
	scanner    *bufio.Scanner
)
var printer = console.ColoredPrinter(1, false).Background(console.BLACK).Foreground(console.WHITE).Build()

func main() {

	scanner = bufio.NewScanner(os.Stdin)

	printer.SetForegroundColor(console.MAGENTA)
	printer.Println("                                     |__")
	printer.Println("                                     |\\/")
	printer.Println("                                     ---")
	printer.Println("                                     / | [")
	printer.Println("                              !      | |||")
	printer.Println("                            _/|     _/|-++'")
	printer.Println("                        +  +--|    |--|--|_ |-")
	printer.Println("                     { /|__|  |/\\__|  |--- |||__/")
	printer.Println("                    +---------------___[}-_===_.'____                 /\\")
	printer.Println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _")
	printer.Println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7")
	printer.Println("|                        Welcome to Battleship                         BB-61/")
	printer.Println(" \\_________________________________________________________________________|")
	printer.Println("")
	printer.SetForegroundColor(console.WHITE)

	initializeGame()

	startGame()
}

func printYourTurn() {

	printer.Println("")
	printEnemyFleetStatistics()
	printer.Println("Player, it's your turn")
	printer.Println("Enter coordinates for your shot :")

}

func printError(msg string) {

	printer.Println("")
	printer.Printf("Error: %s\n", msg)

}

func printNiceHit() {

	printer.Println("Yeah ! Nice hit !")

}

func printMiss() {
	printer.Println("Miss")

	printer.Println("    ~~~~``~-_~_~_~_")
	printer.Println("  ~`~--~__~_~_~_~_~~---          ")
	printer.Println("     ---~___~---~_,,-~~--        ")

}

func printComputerShot(column letter.Letter, row int, result string) {

	printer.Printf("Computer shoot in %s%d and %s\n", column, row, result)

}

func printBomb() {
	beep()

	printer.Println("                \\         .  ./")
	printer.Println("              \\      .:\" \";'.:..\" \"   /")
	printer.Println("                  (M^^.^~~:.'\" \").")
	printer.Println("            -   (/  .    . . \\ \\)  -")
	printer.Println("               ((| :. ~ ^  :. .|))")
	printer.Println("            -   (\\- |  \\ /  |  /)  -")
	printer.Println("                 -\\  \\     /  /-")
	printer.Println("                   \\  \\   /  /")

}

func startGame() {
	printer.Print("\033[2J\033[;H")
	printer.Println("                  __")
	printer.Println("                 /  \\")
	printer.Println("           .-.  |    |")
	printer.Println("   *    _.-'  \\  \\__/")
	printer.Println("    \\.-'       \\")
	printer.Println("   /          _/")
	printer.Println("  |      _  /\" \"")
	printer.Println("  |     /_\\'")
	printer.Println("   \\    \\_/")
	printer.Println("    \" \"\" \"\" \"\" \"")

	for {
		printYourTurn()
		var isHit bool
		for scanner.Scan() {
			input := scanner.Text()
			if input != "" {
				position := parsePosition(input)
				var err error
				isHit, err = controller.TryHit(enemyFleet, position)
				if err != nil {
					printError(err.Error())
				}
				break
			}
		}

		if isHit {
			printBomb()
		}

		if isHit {
			printNiceHit()
		} else {
			printMiss()
		}

		if !controller.IsLivingShipsLeft(enemyFleet) {
			break
		}

		position := getRandomPosition()
		var err error
		isHit, err = controller.TryHit(myFleet, position)
		if err != nil {
			printError(err.Error())
		}
		printer.Println("")

		if !isHit {
			printMiss()
		}
		printComputerShot(position.Column, position.Row, "hit your ship !")
		if isHit {
			printBomb()

		}

		if !controller.IsLivingShipsLeft(myFleet) {
			break
		}
	}
}

func parsePosition(input string) *controller.Position {
	ltr := strings.ToUpper(string(input[0]))
	number, _ := strconv.Atoi(string(input[1]))
	return &controller.Position{
		Column: letter.FromString(ltr),
		Row:    number,
	}
}

func beep() {
	fmt.Print("\007")
}

func initializeGame() {
	initializeMyField()

	initializeEnemyField()

	initializeMyFleet()

	initializeEnemyFleet()
}

func initializeEnemyField() {
	enemyField = &controller.Field{Cols: 8, Rows: 8}
}

func initializeMyField() {
	myField = &controller.Field{Cols: 8, Rows: 8}
}

func initializeMyFleet() {
	//reader := bufio.NewReader(os.Stdin)
	//scanner := bufio.NewScanner(os.Stdin)
	myFleet = controller.InitializeShips()

	printer.Println("Please position your fleet (Game board has size from A to H and 1 to 8) :")
	printer.SetForegroundColor(console.WHITE)

	for _, ship := range myFleet {

		printer.Println("")
		printer.Printf("Please enter the positions for the %s (size: %d)", ship.Name, ship.Size)
		printer.Println("")
		printer.SetForegroundColor(console.WHITE)

		for i := 1; i <= ship.Size; i++ {

			printer.Printf("Enter position %d of %d (i.e A3):\n", i, ship.Size)
			printer.SetForegroundColor(console.WHITE)

			for scanner.Scan() {
				positionInput := scanner.Text()
				if positionInput != "" {
					ship.AddPosition(positionInput)
					break
				}
			}
		}
	}
}

func getRandomPosition() *controller.Position {
	rows := 8
	lines := 8
	letter := letter.Letter(rand.Intn(lines-1) + 1)
	number := rand.Intn(rows-1) + 1
	return &controller.Position{Column: letter, Row: number}
}

const presetsCount = 5

func initializeEnemyFleet() {
	rand.Seed(time.Now().UnixNano())
	presetNumber := rand.Int() % 5

	if presetNumber == 0 {
		initializeEnemyFleet1()
	} else if presetNumber == 1 {
		initializeEnemyFleet2()
	} else if presetNumber == 2 {
		initializeEnemyFleet3()
	} else if presetNumber == 3 {
		initializeEnemyFleet4()
	} else if presetNumber == 4 {
		initializeEnemyFleet5()
	}
	// else if presetNumber == 6 {
	// 	initializeEnemyFleet6()
	// } else if presetNumber == 7 {
	// 	initializeEnemyFleet7()
	// }
}

func initializeEnemyFleet1() {
	enemyFleet = controller.InitializeShips()
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.B, Row: 4})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.B, Row: 5})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.B, Row: 6})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.B, Row: 7})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.B, Row: 8})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.E, Row: 6})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.E, Row: 7})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.E, Row: 8})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.E, Row: 9})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.A, Row: 3})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.B, Row: 3})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.C, Row: 3})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.F, Row: 8})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.G, Row: 8})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 8})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.C, Row: 5})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.C, Row: 6})
}

func initializeEnemyFleet2() {
	enemyFleet = controller.InitializeShips()

	// by dima
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.A, Row: 8})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.B, Row: 8})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.C, Row: 8})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.D, Row: 8})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.E, Row: 8})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 2})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 3})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 4})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 5})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.C, Row: 3})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.D, Row: 3})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.E, Row: 3})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 3})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 4})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 5})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.G, Row: 8})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.H, Row: 8})
}

func initializeEnemyFleet3() {
	enemyFleet = controller.InitializeShips()

	// by sergey
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.A, Row: 1})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.A, Row: 2})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.A, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.A, Row: 4})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.A, Row: 5})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 8})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 8})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.C, Row: 8})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.H, Row: 8})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.H, Row: 7})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.H, Row: 6})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 1})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 2})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 3})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.G, Row: 1})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.F, Row: 2})
}

func initializeEnemyFleet4() {
	enemyFleet = controller.InitializeShips()

	// by renat
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 4})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 5})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 6})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 7})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 5})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 6})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 7})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 8})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.C, Row: 4})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.C, Row: 5})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.C, Row: 6})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.E, Row: 2})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.E, Row: 3})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.E, Row: 4})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.C, Row: 1})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.C, Row: 2})
}

func initializeEnemyFleet5() {
	enemyFleet = controller.InitializeShips()

	// by ya
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 4})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 5})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 6})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 7})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 5})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 5})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.C, Row: 5})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.D, Row: 5})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.C, Row: 2})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.D, Row: 2})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.E, Row: 2})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.F, Row: 4})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.F, Row: 5})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.F, Row: 6})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.B, Row: 8})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.C, Row: 8})
}

func printEnemyFleetStatistics() {
	crashed := make([]string, 0)
	alive := make([]string, 0)
	for _, ship := range enemyFleet {
		if ship.IsCrashed() {
			crashed = append(crashed, fmt.Sprintf("%s (%d)", ship.Name, ship.Size))
		} else {
			alive = append(alive, fmt.Sprintf("%s (%d)", ship.Name, ship.Size))
		}
	}
	printer.SetForegroundColor(console.WHITE)
	printer.Println("Stat:")

	printer.SetForegroundColor(console.RED)
	if len(crashed) == 0 {
		printer.Println("crashed: -")
	} else {
		printer.Println("crashed: " + strings.Join(crashed, ", "))
	}

	printer.SetForegroundColor(console.CADET_BLUE)
	if len(alive) == 0 {
		printer.Println("alive: -")
	} else {
		printer.Println("alive: " + strings.Join(alive, ", "))
	}

	printer.SetForegroundColor(console.WHITE)
}
