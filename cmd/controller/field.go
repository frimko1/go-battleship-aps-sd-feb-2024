package controller

type Field struct {
	Cols int
	Rows int
}

func (f Field) isAllowedPosition(position Position) bool {
	if (position.Row < 1) || (position.Row > f.Rows) {
		return false
	}

	return true
}
